#include <iostream>
#include <string.h>
#include <cmath>
#include "automaton_string.h"

using namespace std;

int main(){
    DFA system;
    system.printDFA();
    cout << system.checkInput("aaaaaaba") << endl;
    return 0;
}