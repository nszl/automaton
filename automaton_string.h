#ifndef AUTOMATON_STRING_H
#define AUTOMATON_STRING_H

#include <iostream>
#include <fstream>
#include <string.h>
#include <cmath>
#include "automaton_string.h"

using namespace std;

/* 

Data structure and representation

The automaton is a type of directed graph where transitions are taken
based on inputs from an alphabet. This is represented by an array of 
linked lists: one list per automaton state. The list of a given state 
contains all possible transitions from that state.

The states variable is an array of Lists, where each list contains the
name of a state, a boolean indicating if the state is accepting, and a
pointer to a list containing possible transitions from the state given
an input from the alphabet.

*/

struct State{
    string dest;
    char input;
    State* next;
};

struct List{
    string name;
    bool isAccepting;
    State* transitions;
};

class DFA{
public:
    // functions to enable DFA setup
    DFA();
    ~DFA();
    void readAlphabet();
    void readStates();
    void readInitialState();
    void readFinalStates();
    void readTransitions();
    State* addNewState(string, char);
    void createTransition(string, char, string);
    // helper functions to display DFA
    void printDFA();
    void printAlphabet();
    void printStates();
    void printInitialState();
    void printFinalStates();
    void printTransitions();
    // functions
    string getTransition(string, char);
    bool checkInput(string);
private:
    int N;      				// number of states
    int letter_count;			// number of letters in alphabet
    string initial_state;
    List* states;
    char* language;
};

#endif