#include <iostream>
#include <string.h>
#include <cmath>
#include "automaton_string.h"

using namespace std;

DFA::DFA(){
    readAlphabet();
    readStates();    
    readInitialState();
    readFinalStates();
    readTransitions();
}

DFA::~DFA(){
    delete[] language;
    delete[] states;
}

void DFA::readAlphabet(){
    // userinput will be used to help parse what the user 
    // inputs to the command line
    string userinput;
    cout << "Enter alphabet separated by spaces:" << endl;
    getline(cin, userinput);
    // count number of letters in alphabet
    letter_count = 0;
    for(int i = 0; i < userinput.length(); i++){
        if(userinput[i] != ' '){
            letter_count++;
        }
    }
    // once the size of the alphabet is known, we can
    // allocate memory for a character array of the right
    // size
    language = new char[letter_count]();
    // populate language with all the non-blank letters
    int j = 0;
    for(int i = 0; i < userinput.length(); i++){
        if(userinput[i] != ' '){
            language[j++] = userinput[i];
        }
    }
}

void DFA::readStates(){
    // userinput will be used to help parse what the user 
    // inputs to the command line
    string userinput;
    // pos will be used to find the position of the blank 
    // spaces in the user input
    size_t pos = 0;
    string delimiter = " ";

    cout << "Enter states separated by spaces:" << endl;
    getline(cin, userinput);
    // count number of states
    N = 1;
    for(int i = 0; i < userinput.length(); i++){
        if(userinput[i] == ' '){
            N++;
        }
    }
    // once the number of states is known, we can allocate
    // memory for a List array of the right size
    states = new List[N];
    // populate states with right state names; also initialize
    // the set of transitions to NULL
    for(int i = 0; i < N; i++){
        pos = userinput.find(delimiter);
        states[i].name = userinput.substr(0, pos);        
        states[i].transitions = NULL;
        userinput.erase(0, pos + delimiter.length());        
    }
}

void DFA::readInitialState(){
    // userinput will be used to help parse what the user 
    // inputs to the command line
    string userinput;
    cout << "Enter initial state:" << endl;
    getline(cin, userinput);
    // assign initial_state
    initial_state = userinput;
}

void DFA::readFinalStates(){
    // userinput will be used to help parse what the user 
    // inputs to the command line
    string userinput;
    // pos will be used to find the position of the blank 
    // spaces in the user input
    size_t pos = 0;
    string delimiter = " ";
    string nameAcceptingState;

    cout << "Enter final states separated by spaces:" << endl;
    getline(cin, userinput);
    // read the name of the accepting state:
    pos = userinput.find(delimiter);
    do{
        // read in name of accepting state
        nameAcceptingState = userinput.substr(0, pos);
        // loop through list containing states and find the state
        // to be designated as accepting
        int stateidx = 0;
        while(states[stateidx].name != nameAcceptingState && stateidx < N){
            stateidx++;
        }
        // update the state to be an accepting state
        states[stateidx].isAccepting = true;
        // update userinput to read next state
        userinput.erase(0, pos + delimiter.length());   
    } while ((pos = userinput.find(delimiter)) != std::string::npos);
}

State* DFA::addNewState(string dest, char input){
    State* stateptr = new State;
    // set the new destination to be a destination of the
    // temporary stateptr
    stateptr->dest = dest;
    // the destination is reached by applying input
    stateptr->input = input;
    stateptr->next = NULL;
    
    return stateptr;
}

void DFA::createTransition(string src, char input, string dest){
    // create newState with the right dest and input
    State* newState = addNewState(dest, input);
    // find the state corresponding to src
    int srcidx = 0;
    while(states[srcidx].name != src && srcidx < N){
        srcidx++;
    }
    // link the end of newState to the beginning of src's list of
    // transitions
    newState->next = states[srcidx].transitions;
    // now update src's list of transitions with newState
    states[srcidx].transitions = newState;
}

void DFA::readTransitions(){
    string userinput;
    string dest;
    for(int i = 0; i < N; i++){
        for(int j = 0; j < letter_count; j++){
            cout << "Enter transition for state " << states[i].name
                << " with input " << language[j] << ":"<< endl;
            cin >> dest;
            createTransition(states[i].name, language[j], dest);
        }
    }
}

void DFA::printAlphabet(){
    cout << "DFA alphabet: ";
    for(int i = 0; i < letter_count; i++){
        cout << language[i] << " ";
    }
    cout << endl;
}

void DFA::printStates(){
    cout << "DFA states: ";
    for(int i = 0; i < N; i++){
        cout << states[i].name << " ";
    }
    cout << endl;
}

void DFA::printInitialState(){
    cout << "DFA initial state: " << initial_state << endl;
}

void DFA::printFinalStates(){
    cout << "DFA final states: ";
    for(int i = 0; i < N; i++){
        if (states[i].isAccepting)
        cout << states[i].name << " ";
    }
    cout << endl;
}


string DFA::getTransition(string src, char input){
    // pointer to source's transition list
    State* sourceptr;
    int srcidx = 0;
    while(states[srcidx].name != src && srcidx < N){
        srcidx++;
    }
    sourceptr = states[srcidx].transitions;
    while(sourceptr->input != input){
        sourceptr = sourceptr->next;
    }
    return sourceptr->dest;
}

void DFA::printTransitions(){
    cout << "DFA transitions: " << endl;
    for(int i = 0; i < N; i++){
        for(int j = 0; j < letter_count; j++){
            cout << states[i].name << "--" << language[j] << "->" <<
            getTransition(states[i].name, language[j]) << endl;
        }
    }
}

void DFA::printDFA(){
    printAlphabet();
    printStates();
    printInitialState();
    printFinalStates();
    printTransitions();
}

bool DFA::checkInput(string inputString){
    string currentState = initial_state;
    char input;
    // execute inputString
    for(int i = 0; i < inputString.length(); i++){
        input = inputString[i];
        currentState = getTransition(currentState, input);
    }
    // check if the state is an accepting state
    for(int i = 0; i < N; i++){
        if(states[i].name == currentState && states[i].isAccepting)
            return true;        
    }
    return false;
}
